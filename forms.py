from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import URL, DataRequired, EqualTo, Length

# Set your classes here.


class RegisterForm(Form):
    name = TextField(
        'Username', validators=[DataRequired(), Length(min=6, max=25)]
    )
    email = TextField(
        'Email', validators=[DataRequired(), Length(min=6, max=40)]
    )
    password = PasswordField(
        'Password', validators=[DataRequired(), Length(min=6, max=40)]
    )
    confirm = PasswordField(
        'Repeat Password',
        [DataRequired(),
        EqualTo('password', message='Passwords must match')]
    )


class LoginForm(Form):
    name = TextField('Username', [DataRequired()])
    password = PasswordField('Password', [DataRequired()])


class ForgotForm(Form):
    email = TextField(
        'Email', validators=[DataRequired(), Length(min=6, max=40)]
    )

class ProjectForm(Form):
    projects = TextField('ProjectName', [DataRequired()])


class ServicesForm(Form):
    name = TextField('name', [DataRequired()])
    monitor_url = TextField('monitor_url', validators=[ URL(require_tld=True, message="Error format url")])
    influxdb_server = TextField('influxdb_server', [DataRequired()])

class GrafanaSetupForm(Form):
    name = TextField('grafana_service_name', [DataRequired()])
    url = TextField('grafana_url', [DataRequired()])
    username = TextField('grafana_username', [DataRequired()])
    password  = TextField('grafana_password', [DataRequired()] )
    token  = TextField('grafana_token')
