#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, flash, render_template, request, abort, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy

from sqlalchemy.exc import IntegrityError
from sqlalchemy import exc

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
import logging
from logging import Formatter, FileHandler
from models import *
from forms import *
import os

import pprint

import validators, urllib3
import subprocess
from subprocess import Popen

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

# migrate = Migrate(app,db)
# manager = Manager(app)
# manager.add_command('db', MigrateCommand)




# Automatically tear down SQLAlchemy.
'''
@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()
'''

# Login required decorator.
'''
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap
'''
#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#


@app.route('/')
def home():

    return render_template('pages/placeholder.home.html',
                          items = Monitor.query.all()
                          )


@app.route('/help')
def help():
    return render_template('pages/placeholder.help.html')


@app.route('/login')
def login():
    form = LoginForm(request.form)
    return render_template('forms/login.html', form=form)


@app.route('/sg', methods=['GET', 'POST'])
def setup_grafana():
    form = GrafanaSetupForm(request.form)
    if request.method == 'POST':
       grafana_node = GrafanaSetup(form.name.data,
                              form.url.data,
                              form.username.data,
                              form.password.data,
                              form.token.data)
       pprint.pprint(form.url.data)
       db.session.add(grafana_node)
       db.session.commit()
       try:
           pass
           # db.session.commit()
       except exc.IntegrityError:
           flash("Error :) ")
           return render_template('forms/setup_grafana.html')
       return  redirect(url_for('home'))
    items = GrafanaSetup.query.all()
    return render_template('forms/setup_grafana.html', form=form, items=items)


@app.route('/dp')
def deploy_grafana_dashboard():
    form = ForgotForm(request.form)
    return render_template('forms/forgot.html', form=form)

# @udomsak add

@app.route('/projects')
def projects():
    form = ProjectForm(request.form)
    return render_template('forms/register_project', form=form)

@app.route('/dp')
def services():
    form = ServicesForm(request.form)
    return render_template('forms/register-services.html', form=form)

@app.route('/ops/', methods=['POST'])
def ops():
    if request.method == 'POST':
       r = request.form['ops-switch']
       id, ops = r.split('_')
       user_ops = Monitor.query.filter_by(id=id).first()
       if user_ops.enable == True:
           sp = Popen(["echo", "Stop container ",user_ops.name])
           sp = Popen(["docker-compose", "stop"],cwd=user_ops.name)
           user_ops = Monitor.query.filter_by(id=id).first()
           enable_container = False
           user_ops.enable = enable_container
           db.session.commit()

           return render_template('pages/placeholder.home.html',
                                  item = Monitor.query.all()
                                 )

       http = urllib3.PoolManager()
       try:
           resp = http.request('GET', user_ops.url)
       except urllib3.exceptions.HTTPError as e:
           flash("Can not access Nginx to monitor with given URL")
           return redirect(url_for('home'))

       except urllib3.exceptions.ConnectionError as e:
           flash("connection error please check remote URL")
       else:
           Popen(["mkdir", "-pv", user_ops.name])
           # ./bin/telegraf --config conf/telegraf.conf
           Popen(["cp", "docker-compose.yml", user_ops.name])
           org_name = "Nginx test"
           service_name = "Load Balance"
           container_name = user_ops.name
           sp = Popen(["docker-compose up"],cwd=user_ops.name ,shell=True, bufsize=200, env=dict(SERVICE_NAME=user_ops.name,
                                                                                     ORG_NAME='My Org',
                                                                                     HOSTNAME=user_ops.name,
                                                                                     REMOTE_NGINX="'" + user_ops.url +"'",
                                                                                     INFLUXDB_SERVER="'" + user_ops.influxdb_server + "'"))
           sp.communicate()[0]
           child_process_code = sp.returncode
           if not child_process_code == 0:
               flash("Have problem while start new monitor node service check docker! ")
               return render_template('pages/placeholder.home.html',
                                      item = Monitor.query.all()
                                     )
           enable_container = True
           user_ops = Monitor.query.filter_by(id=1).first()
           user_ops.enable = enable_container
           db.session.commit()
           user_ops = Monitor.query.filter_by(id=1).first()

           return render_template('pages/placeholder.home.html',
                             items = Monitor.query.all()
                             )

@app.route('/d/<int:item_id>', methods=['GET','POST'])
def destroy(item_id):
    if request.method == 'GET':
        delete_monitor = Monitor.query.filter_by(id=item_id).first()
        sp = Popen(["docker-compose", "down"], cwd=delete_monitor.name)
        sp.communicate()[0]
        child_process_code = sp.returncode
        if not child_process_code == 0:
            flash("Docker-compose can not Shutdown monitor node name: " + delete_monitor.name)
            return url_for('home')
        sp = Popen(["rm", "-rf", delete_monitor.name])
        return url_for('home')

@app.route('/e/<int:item_id>', methods=['GET', 'POST'])
def show_or_update(item_id):
    monitor_items = Monitor.query.filter_by(id=item_id).first()
    old_monitor_run = monitor_items.name
    form = ServicesForm(obj=monitor_items)
    pprint.pprint(monitor_items.url)
    if request.method == 'GET':
        return render_template('forms/update.html', form=monitor_items)

    monitor_items.name = form.name.data
    if not monitor_items.name == old_monitor_run:
        sp = Popen(["docker-compose", "down"], cwd=old_monitor_run)
        sp = Popen(["mv", old_monitor_run, monitor_items.name])
    monitor_items.url = form.monitor_url.data
    monitor_items.influxdb_server = form.influxdb_server.data
    sp = Popen(["docker-compose up"],cwd=monitor_items.name ,shell=True, bufsize=200, env=dict(SERVICE_NAME=monitor_items.name,
                                                                                         ORG_NAME='Unixdev.',
                                                                                         HOSTNAME=monitor_items.name,
                                                                                         REMOTE_NGINX="'" + monitor_items.url +"'",
                                                                                         INFLUXDB_SERVER="'" + monitor_items.influxdb_server + "'"))

    sp.communicate()[0]
    child_process_code = sp.returncode
    if not child_process_code == 0:
        flash("Docker-compose can not Start monitor node name: " + delete_monitor.name + "After Upgrade! please check")

        return url_for('home')

    db.session.commit()
    return redirect(url_for('home'))


@app.route('/new', methods=['GET', 'POST'])
def new():
    form = ServicesForm(request.form)
    if request.method == 'POST':

       service = Monitor(form.name.data, form.monitor_url.data, form.influxdb_server.data)
       db.session.add(service)
       try:
           db.session.commit()
       except exc.IntegrityError:
           flash("This URL already exist :) ")
           return render_template('forms/monitor2.html')
       return  redirect(url_for('home'))

    return render_template('forms/monitor2.html', form=form)

# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    #db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
#    manager.run()
    app.run(host='0.0.0.0', port=5001)

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='0.0.0.0', port=port)
'''
