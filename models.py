from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from app import db

engine = create_engine('sqlite:///database.db', echo=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

# Set your classes here.


class User(Base):
    __tablename__ = 'Users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(30))

    def __init__(self, name=None, password=None):
        self.name = name
        self.password = password

class Project(Base):
    __tablename__ = 'Projects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    user_id = db.Column(db.Integer)

    def __init__(self, name=None, user_id=None):
        self.name = name
        self.user_id = user_id

class Service(Base):
    __tablename__ = 'Services'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    project_id = db.Column(db.Integer)

    def __init__(self, name=None, project_id=None):
        self.name = name
        self.project_id = project_id


class Monitor(Base):
    __tablename__ = 'Monitor'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60))
    url = db.Column(db.String, unique=True)
    influxdb_server = db.Column(db.String, nullable=False)
    enable = db.Column(db.Boolean)
    service_id = db.Column(db.Integer)

    def __init__(self, name=None, url=None, influxdb_server=None, enable=0, service_id=1):
        self.name = name
        self.url = url
        self.influxdb_server = influxdb_server
        self.enable = enable
        self.service_id = service_id

class GrafanaSetup(Base):
    __tablename__ = 'GrafanaSetup'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    url = db.Column(db.String, nullable=False )
    username = db.Column(db.String)
    password = db.Column(db.String)
    token = db.Column(db.String)

    def __init__(self, name=None, url=None, username=None, password=None, token=None):
        self.name = name
        self.url = url
        self.username = username
        self.password = password
        self.token = token




# Create tables.
Base.metadata.create_all(bind=engine)
